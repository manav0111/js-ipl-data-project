// Import required modules
const fs = require("fs");
const csv = require("csvtojson");
const path = require("path");

// Specify the path for the CSV file
const csvFilePath = "/home/manav/IPL Project/data/deliveries.csv";

// Parse CSV file to JSON
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // Display the parsed JSON data
    console.log(jsonObj);

    // Create an object to store the count of dismissals for each player
    const playerDismed = {};

    // Iterate through the JSON data to count dismissals for each player
    for (let index = 0; index < jsonObj.length; index++) {
      const cricketEvent = jsonObj[index];

      // Check if a player has been dismissed
      if (cricketEvent.player_dismissed !== "") {
        // If player is not in the object, initialize count to 1
        if (!playerDismed[cricketEvent.player_dismissed]) {
          playerDismed[cricketEvent.player_dismissed] = 1;
        } else {
          // If player is already in the object, increment the count
          playerDismed[cricketEvent.player_dismissed] += 1;
        }
      }
    }

    // Find the player with the highest number of dismissals
    let highestDismed = {};
    let maximumDismed = 0;
    for (let key in playerDismed) {
      if (playerDismed[key] > maximumDismed) {
        maximumDismed = playerDismed[key];
        highestDismed = { [key]: maximumDismed };
      }
    }

    // Display the player dismissal count object
    console.log(playerDismed);

    // Display the player with the highest dismissal count
    console.log(highestDismed);

    // Specify the path for the output JSON file
    const result = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "highest-time-one-player-dismissed.json"
    );

    // Write the data into a JSON file
    fs.writeFileSync(result, JSON.stringify(highestDismed, null, 2));
  });

// Extra runs conceded per team in the year 2016

const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");

csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    const obj = {};
    const tempobj = [];

    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];

      //checking for the season which are in year 2016

      if (object.season == 2016) {
        //pushing the id's of an matches into temp obj
        tempobj.push(Number(object.id));
      }
    }

    //given the path for deliveries file
    const csvFilePathDeliveries = "/home/manav/IPL Project/data/deliveries.csv";

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((deliveryjsonObj) => {
        //Now Traversing the delivery object
        for (let index = 0; index < deliveryjsonObj.length; index++) {
          const Object = deliveryjsonObj[index];

          //Now checking the objects id's of delivery which are there in tempobj which conatins id's of 2016

          if (tempobj.includes(Number(Object.match_id))) {
            if (!obj[Object.batting_team]) {
              obj[Object.batting_team] = Number(Object.extra_runs);
            } else {
              obj[Object.batting_team] += Number(Object.extra_runs);
            }
          }
        }
        //Given the path of json file
        console.log(obj);
        const result = path.join(
          __dirname,
          "..",
          "public",
          "output",
          "extra-run-per-team.json"
        );

        console.log(path);

        //Now write this data into json file
        fs.writeFileSync(result, JSON.stringify(obj, null, 2));
      });
  });

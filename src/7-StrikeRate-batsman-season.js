// Find the strike rate of a batsman for each season

// Import required modules
const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");

// Read matches data and create a mapping of match_id to season
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    const obj = {};

    // Create a mapping of match_id to season
    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];
      obj[object.id] = object.season;
    }

    // Initialize data structures to store batsman statistics
    let batsmenTotalBalls = {};
    let batsmanTotalRuns = {};

    // Given the path for deliveries file
    const csvFilePathDeliveries = "/home/manav/IPL Project/data/deliveries.csv";

    // Read deliveries data and calculate total runs and balls for each batsman in each season
    csv()
      .fromFile(csvFilePathDeliveries)
      .then((deliveryjsonObj) => {
        // Now traverse the delivery object
        for (let index = 0; index < deliveryjsonObj.length; index++) {
          const deliveryObject = deliveryjsonObj[index];

          let season = obj[deliveryObject.match_id];

          if (!batsmanTotalRuns[season]) {
            batsmanTotalRuns[season] = {};
            batsmenTotalBalls[season] = {};
          }
          if (!batsmanTotalRuns[season][deliveryObject.batsman]) {
            batsmanTotalRuns[season][deliveryObject.batsman] = Number(
              deliveryObject.batsman_runs
            );
            batsmenTotalBalls[season][deliveryObject.batsman] = 1;
          } else {
            batsmanTotalRuns[season][deliveryObject.batsman] += Number(
              deliveryObject.batsman_runs
            );
            batsmenTotalBalls[season][deliveryObject.batsman] += 1;
          }
        }

        // Uncomment the following lines for debugging purposes
        // console.log(batsmanTotalRuns);
        // console.log(batsmenTotalBalls);

        // Initialize data structure to store batsman strike rates
        let batsmenStrikeRate = {};

        // Read deliveries data again to calculate batsman strike rates
        csv()
          .fromFile(csvFilePathDeliveries)
          .then((deliveryjsonObj) => {
            // Now traverse the delivery object
            for (let index = 0; index < deliveryjsonObj.length; index++) {
              const deliveryObject = deliveryjsonObj[index];

              let season = obj[deliveryObject.match_id];

              if (!batsmenStrikeRate[season]) {
                batsmenStrikeRate[season] = {};
              }

              if (!batsmenStrikeRate[season][deliveryObject.batsman]) {
                batsmenStrikeRate[season][deliveryObject.batsman] = (
                  (batsmanTotalRuns[season][deliveryObject.batsman] /
                    batsmenTotalBalls[season][deliveryObject.batsman]) *
                  100
                ).toFixed(2);
              }
            }
            return batsmenStrikeRate;
          })
          .then((batsmenStrikeRate) => {
            // Log the calculated strike rates
            console.log(batsmenStrikeRate);

            // Specify the path for the output JSON file
            const result = path.join(
              __dirname,
              "..",
              "public",
              "output",
              "strike-rate-batsman-season.json"
            );

            // Log the path (optional)
            console.log(result);

            // Write the data into a JSON file
            fs.writeFileSync(result, JSON.stringify(batsmenStrikeRate, null, 2));
          });
      });
  });

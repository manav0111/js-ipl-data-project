// Find the bowler with the best economy in super overs
// Import required modules
const fs = require("fs");
const csv = require("csvtojson");
const path = require("path");

// Specify the path for the CSV file
const csvFilePath = "/home/manav/IPL Project/data/deliveries.csv";

// Parse CSV file to JSON
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // Display the parsed JSON data
    console.log(jsonObj);

    let bowlerTotalBalls = {};
    let bowlerTotalRunsGiven = {};


    for(let index=0; index<jsonObj.length; index++)
    {
        const Object=jsonObj[index];

        if(Object.is_super_over=="1")
        {   
            if(!bowlerTotalRunsGiven[Object.bowler])
            {


                bowlerTotalRunsGiven[Object.bowler]=Number(Object.total_runs-Object.legbye_runs-Object.bye_runs);

            }
            else
            {
                bowlerTotalRunsGiven[Object.bowler]+=Number(Object.total_runs-Object.legbye_runs-Object.bye_runs);
            }

        }

    }



    for(let index=0; index<jsonObj.length; index++)
    {
        const Object=jsonObj[index];

        if(Object.is_super_over=="1")
        {   
            if(!bowlerTotalBalls[Object.bowler])
            {


                bowlerTotalBalls[Object.bowler]=1;

            }
            else
            {
                bowlerTotalBalls[Object.bowler]+=1;
            }

        }

    }

    console.log("Total Balls:",bowlerTotalBalls);
    console.log("Total Runs:",bowlerTotalRunsGiven);


    const SuperOverEconomy={};

    for(let key in bowlerTotalRunsGiven)
    {
        if(!SuperOverEconomy[key])
        {
            SuperOverEconomy[key]=((bowlerTotalRunsGiven[key]/bowlerTotalBalls[key])*6).toFixed(2);
        }
    }



    console.log("Economy:",SuperOverEconomy);

    let SuperOverEconomyArray=[];
    for (let key in SuperOverEconomy) {
        SuperOverEconomyArray.push([key, SuperOverEconomy[key]]);
      }

      SuperOverEconomyArray.sort((value1, value2) => {
        return value1[1] - value2[1];
      });

      console.log(SuperOverEconomyArray);


   
      const HighestSuperOverEconomy={
        BowlerName:SuperOverEconomyArray[0][0],
        Economy:SuperOverEconomyArray[0][1],
      };
    
      

      console.log(HighestSuperOverEconomy);

    
          // Specify the path for the output JSON file
          const result = path.join(
            __dirname,
            "..",
            "public",
            "output",
            "BowlerEconomy-Superover.json"
          );
  
  
          // Write the data into a JSON file
          fs.writeFileSync(result, JSON.stringify(HighestSuperOverEconomy, null, 2));
    


  })
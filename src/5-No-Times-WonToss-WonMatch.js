// Import required modules
const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");

// Read CSV file and convert it to JSON
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {

    // Initialize an object to store the count of teams winning both toss and match
    const obj = {};

    // Loop through each match record in the JSON data
    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];

      // Extract toss winner and match winner for each match
      const TossWinner = object.toss_winner;
      const MatchWinner = object.winner;

      // Check if the toss winner is the same as the match winner
      if (TossWinner == MatchWinner) {
        // If the team has not been encountered before, initialize the count to 1
        if (!obj[TossWinner]) {
          obj[TossWinner] = 1;
        } else {
          // If the team has been encountered before, increment the count
          obj[TossWinner] = obj[TossWinner] + 1;
        }
      }
    }

    // Log the result object to the console
    console.log(obj);

    // Define the output file path
    const result = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "no-times-wontoss-wonmatch.json"
    );


    // Write the result object to a JSON file
    fs.writeFileSync(result, JSON.stringify(obj, null, 2));
  });

// Number of matches played per year for all the years in IPL.

const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");
const { match } = require("assert");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    console.log(jsonObj);

    const obj = {};

    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];

    
        const season=object.season;
        const player=object.player_of_match;

      if (!obj.hasOwnProperty(season)) {
             obj[season]={};
      } 

      if(!obj[season][player])
      {
        obj[season][player]=1;
      }
      else
      {
        obj[season][player]++;
      }
    }

    // console.log(obj);

    const result={};
    
    for(let key in obj)
    {
        let playerofthematch;
        let maximum=0;
        const playerlist=obj[key];
        for(let key in playerlist)
        {
            if(playerlist[key]>maximum)
            {
                maximum=playerlist[key];
                playerofthematch=key;

            }
        }

        result[key]={playerofthematch,"No of Time":maximum};
    }


    console.log(result);


    const filePath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "player-highest-match-season.json"
    );

    console.log(path);

    fs.writeFileSync(filePath, JSON.stringify(result, null, 2));
  });

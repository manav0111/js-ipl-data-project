// Number of matches played per year for all the years in IPL.

const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");
const { match } = require("assert");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    console.log(jsonObj);

    const obj = {};

    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];

    
        const season=object.season;
        const winner=object.winner;

      if (!obj.hasOwnProperty(winner)) {
             obj[winner]={};
      } 

      if(!obj[winner][season])
      {
        obj[winner][season]=1;
      }
      else
      {
        obj[winner][season]++;
      }
    }

    console.log(obj);

    const result = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "matches-won-per-team-per-year.json"
    );

    console.log(path);

    fs.writeFileSync(result, JSON.stringify(obj, null, 2));
  });

// Number of matches played per year for all the years in IPL.

const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    console.log(jsonObj);

    const obj = {};

    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];

      if (obj.hasOwnProperty(object.season)) {
        obj[object.season] = obj[object.season] + 1;
      } else {
        obj[object.season] = 1;
      }
    }

    console.log(obj);

    const result = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "matchesPerYear.json"
    );

    console.log(path);

    fs.writeFileSync(result, JSON.stringify(obj, null, 2));
  });

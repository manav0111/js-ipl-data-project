// Import required modules
const fs = require("fs");
const csvFilePath = "/home/manav/IPL Project/data/matches.csv";
const csv = require("csvtojson");
const path = require("path");

// Read data from matches.csv file
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    const Matchidobj = [];

    // Create a mapping of match_id to season for matches in 2015
    for (let index = 0; index < jsonObj.length; index++) {
      let object = jsonObj[index];
      if (object.season == "2015") {
        Matchidobj.push(object.id);
      }
    }

    // Array to store delivery details of matches in 2015
    let matchesOf2015 = [];

    // Path for deliveries.csv file
    const csvFilePathDeliveries = "/home/manav/IPL Project/data/deliveries.csv";

    // Read data from deliveries.csv file
    csv()
      .fromFile(csvFilePathDeliveries)
      .then((deliveryjsonObj) => {
        // Filter delivery data for matches in 2015
        for (let index = 0; index < deliveryjsonObj.length; index++) {
          const deliveryObject = deliveryjsonObj[index];

          if (Matchidobj.includes(deliveryObject.match_id)) {
            matchesOf2015.push(deliveryObject);
          }
        }

        // Object to store runs given by bowlers
        let bowlerRunsGiven = {};

        // Calculate runs given by each bowler
        for (let index = 0; index < matchesOf2015.length; index++) {
          let Object = matchesOf2015[index];

          if (!bowlerRunsGiven[Object.bowler]) {
            bowlerRunsGiven[Object.bowler] = Number(Object.total_runs);
          } else {
            bowlerRunsGiven[Object.bowler] += Number(Object.total_runs);
          }
        }

        // Object to store overs bowled by each bowler
        let bowlerOvers = {};
        for (let index = 0; index < matchesOf2015.length; index++) {
          let Object = matchesOf2015[index];

          if (!bowlerOvers[Object.bowler]) {
            bowlerOvers[Object.bowler] = 1;
          } else {
            bowlerOvers[Object.bowler] += 1;
          }
        }

        // Object to store economy rate of each bowler
        let bowlerEconomyObject = {};

        // Calculate economy rate of each bowler
        for (let index = 0; index < matchesOf2015.length; index++) {
          let Object = matchesOf2015[index];

          if (!bowlerEconomyObject[Object.bowler]) {
            bowlerEconomyObject[Object.bowler] = (
              (bowlerRunsGiven[Object.bowler] / bowlerOvers[Object.bowler]) *
              6
            ).toFixed(2);
          }
        }

        // Array to store bowler names and their economy rates
        let bowlerEconomyArray = [];

        for (let key in bowlerEconomyObject) {
          bowlerEconomyArray.push([key, bowlerEconomyObject[key]]);
        }

        // Sort bowlers based on economy rate in ascending order
        bowlerEconomyArray.sort((value1, value2) => {
          return value1[1] - value2[1];
        });

        // Display the sorted array of bowler names and economy rates
        console.log(bowlerEconomyArray);

        // Select the top 10 bowlers with the lowest economy rates
        let top10BowlerEconomies = bowlerEconomyArray.slice(0, 10);

        // Display the top 10 bowlers and their economy rates
        console.log("result:", top10BowlerEconomies);

        // Specify the path for the output JSON file
        const result = path.join(
          __dirname,
          "..",
          "public",
          "output",
          "top10-Bowler-Economics.json"
        );

        // Log the path (optional)
        console.log(result);

        // Write the data into a JSON file
        fs.writeFileSync(result, JSON.stringify(top10BowlerEconomies, null, 2));
      });
  });
